#!/usr/bin/env python3

"""

"""

# STDLIB IMPORTS
from argparse import ArgumentParser
from os import getcwd, mkdir
from os.path import dirname, exists, isdir, join, realpath
from sys import exit

# THIRD PARTY IMPORTS
from jinja2 import Environment, FileSystemLoader


def _parse_args():
    """
    Parse and return the command line arguments
    passed into the script.
    """
    _parser = ArgumentParser()

    _parser.add_argument(
        "-d", "--directory", help="Directory to create lab skeleton. Defaults to current directory.", default=getcwd()
    )
    _parser.add_argument("-l", "--lab-name", help="Name of the Lab", required=True)
    _parser.add_argument("-n", "--lab-number", type=int, help="Lab Number")
    _parser.add_argument("-q", "--num-questions", type=int, help="Number of questions the lab contains", required=True)
    _parser.add_argument("-s", "--synopsis", action="store_true")

    return _parser.parse_args()


def _create_dirctories(out_dir: str):
    """Create the directories that will hold the
    lab write-ups.

    :param out_dir: Output directory to be created.
    :type out_dir: str
    """
    # assumes write privileges are available
    if isdir(out_dir):
        return

    if exists(out_dir):
        print(f"{out_dir} already exists, but is not a directory.")
        exit(1)

    try:
        mkdir(path=out_dir)
        mkdir(path=join(out_dir, "images"))
    except Exception as e:
        print(e)
        exit(1)


def _create_markdown(lab_name: str, lab_num: int, num_questions: int, out_dir: str, add_synopsis: bool):
    """Create the lab write-up markdown.

    :param lab_name: Name of the lab.
    :type lab_name: str

    :param lab_num: Number of the lab on the site.
    :type lab_num: int

    :param num_questions: Number of questions a lab
        contains.
    :type num_questions: int

    :param out_dir: Location of where the markdown
        file will be created.
    :type out_dir: str

    :param add_synopsis: Add synopsis section to
        markdown file.
    :type add_synopsis: bool
    """
    env = Environment(loader=FileSystemLoader(dirname(realpath(__file__))))
    template = env.get_template("lab_skel.j2")
    filename = f"{lab_num}-{lab_name}.md"

    rendered = template.render(lab_name=lab_name, num_questions=num_questions, add_synopsis=add_synopsis)
    with open(join(out_dir, filename), mode="w") as md:
        md.writelines(rendered)


if __name__ == "__main__":
    args = _parse_args()

    out_dir = join(args.directory, f"{args.lab_number}-{args.lab_name}")

    _create_dirctories(out_dir=out_dir)
    _create_markdown(
        lab_name=args.lab_name,
        lab_num=args.lab_number,
        num_questions=args.num_questions,
        out_dir=out_dir,
        add_synopsis=args.synopsis,
    )
